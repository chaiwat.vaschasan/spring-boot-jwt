package com.project.authentication.models;

import com.project.authentication.entities.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AccountDetails implements UserDetails {

    private int Id;
    private String username;
    private String password;
    private String email;
    private String role;
    private Collection<? extends GrantedAuthority> authorities;

    public AccountDetails(){}

    public static AccountDetails build(Account account){

        List<GrantedAuthority> authorities = new ArrayList<>();
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(account.getRole());
        authorities.add(simpleGrantedAuthority);

        return new AccountDetails(
                account.getId(),
                account.getUsername(),
                account.getPassword(),
                account.getEmail(),
                account.getRole(),
                authorities
        );
    }

    public AccountDetails(int id, String username, String password, String email, String role, Collection<? extends GrantedAuthority> authorities) {
        Id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = role;
        this.authorities = authorities;
    }

    public int getId() {
        return Id;
    }

    public String getEmail() {
        return email;
    }

    public String getRole() {
        return role;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
