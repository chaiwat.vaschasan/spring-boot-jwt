package com.project.authentication.models;


import java.util.Date;

public class JwtResponse {

    private String access;
    private String reface;
    private Date expire;
    private String type;

    public JwtResponse() {
    }

    public JwtResponse(String access, String reface, Date expire) {
        this.access = access;
        this.reface = reface;
        this.expire = expire;
        this.type = "Bearer";
    }

    public String getType() {
        return type;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getReface() {
        return reface;
    }

    public void setReface(String reface) {
        this.reface = reface;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }
}
