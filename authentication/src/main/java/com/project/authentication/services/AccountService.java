package com.project.authentication.services;

import com.project.authentication.entities.Account;
import com.project.authentication.models.AccountDetails;
import com.project.authentication.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Account account = accountRepository.findByUsername(s)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + s));

        return AccountDetails.build(account);
    }

    @Transactional
    public void addAccount(Account account) throws Exception {

        if(accountRepository.existsByUsername(account.getUsername()))
            throw new Exception("Error: Username is already taken!");
        if(accountRepository.existsByEmail(account.getEmail()))
            throw new Exception("Error: Email is already in use!");

        account.setPassword(encoder.encode(account.getPassword()));
        accountRepository.save(account);
    }


}
