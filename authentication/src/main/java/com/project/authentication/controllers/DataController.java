package com.project.authentication.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(value = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
@PreAuthorize("isAuthenticated()")
public class DataController {

    @GetMapping("/data")

    public String Data(){
        return "TEST DATA";
    }

    @GetMapping("/dataAdmin")
    @PreAuthorize("hasRole('ADMIN')")
    public String DataAdmin(){
        return "DATA ADMIN";
    }

    @GetMapping("/dataUser")
    @PreAuthorize("hasRole('USER')")
    public String DataUser(){
        UserDetails userDetails =
                (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return "TEST USER";
    }
}
