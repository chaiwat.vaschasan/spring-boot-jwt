package com.project.authentication.controllers;

import com.project.authentication.entities.Account;
import com.project.authentication.models.AccountDetails;
import com.project.authentication.models.JwtResponse;
import com.project.authentication.models.LoginRequest;
import com.project.authentication.services.AccountService;
import com.project.authentication.services.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/auth/")
public class AuthController {

    @Autowired
    private AccountService service;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Value("${jwtExpirationMs}")
    private int jwtExpirationMs;

    @PostMapping("/signup")
    public ResponseEntity<?> registerAccount(@Valid @RequestBody Account account){
        try{
            service.addAccount(account);
            return ResponseEntity.ok("Account registered successfully!");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/singin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest){
        try{
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
            String jwt = jwtService.generateJwtToken(authentication);
            AccountDetails user = (AccountDetails) authentication.getPrincipal();
            Date date = new Date((new Date()).getTime() + jwtExpirationMs);
            JwtResponse response = new JwtResponse(jwt,"",date);
            return ResponseEntity.ok(response);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
